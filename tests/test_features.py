
"""
Note: These tests will fail if you have not first trained the model.
"""
import sys
from pathlib import Path
file = Path(__file__).resolve()
parent, root = file.parent, file.parents[1]
sys.path.append(str(root))

import numpy as np
from bikeshare_model.config.core import config
from bikeshare_model.processing.features import WeekdayImputer


def test_weekday_variable_transformer(sample_input_data):
    # Given
    transformer = WeekdayImputer(
        variables=config.model_config_data.weekdayimputer_var,  # weekday
    )
    # assert np.isnan(sample_input_data.loc[708,'weekday'])
    assert np.isnan(sample_input_data.loc[708,'weekday'])

    # When
    subject = transformer.fit(sample_input_data).transform(sample_input_data)

    # Then
    assert subject.loc[708,'weekday'] == 'Wed'