from typing import Any, List, Optional

from pydantic import BaseModel
from bikeshare_model.processing.validation import DataInputSchema


class PredictionResults(BaseModel):
    errors: Optional[Any]
    version: str
    #predictions: Optional[List[int]]
    predictions: Optional[int]


class MultipleDataInputs(BaseModel):
    inputs: List[DataInputSchema]

    class Config:
        schema_extra = {
            "example": {
                "inputs": [
                    {
                        'dteday':'2012-09-28',
                        'season':'winter',
                        'hr':'7am',
                        'holiday':'No',
                        'weekday':'Fri',
                        'workingday':'Yes',
                        'weathersit':'Mist',
                        'temp': 18.32,
                        'atemp':18.9998,
                        'hum':94.0,
                        'windspeed':0.0,
                        'casual': 10,
                        'registered':384
                    }
                ]
            }
        }
